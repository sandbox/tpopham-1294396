/**
 * The <a href="http://drupal.org/project/features">Features</a> module covers
 * version control of content types very well. Fields and field instances are
 * especially well suited to import/export since actual names are used to
 * associate their data. Contrarily, taxonomies use internal identifiers that are
 * local to each installation. Features are intended to facilitate import/export
 * across multiple sites, but the taxonomy field data carries references to the
 * internal identifiers, complicating matters. If the taxonomy tables are naively
 * reverted to a previous state, then taxonomy term references to newer terms
 * could become orphaned.
 *
 * Excluding nodes from version control and including a taxonomy represent the
 * sought workflow. As components to the strategy, I imagine bracketing the term
 * ids used during each version and preventing new terms from getting inserted in
 * the union of the brackets. Adding terms is relatively uncommon for most sites,
 * so there should be no difference in performance. Additionally, an embellished
 * term reference field can hold the place of term references that have changed
 * between versions. The administrative interface should permit the removal of
 * particular versions, merging of fields, unmerging of fields, ...to be
 * continued.
 */
